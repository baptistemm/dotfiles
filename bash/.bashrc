# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
if ! [[ "$PATH" =~ "/usr/local/go/bin" ]]
then
    PATH="/usr/local/go/bin:$PATH"
fi
export PATH

[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
      . /usr/share/bash-completion/bash_completion

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

export TERM=xterm-256color
export EDITOR=vim

eval $(keychain --eval)

for k in ~/.ssh/*.pub; do ssh-add "${k/.pub/}"; done

GIT_PROMPT_ONLY_IN_REPO=1
source ~/.bash-git-prompt/gitprompt.sh
override_git_prompt_colors() {
  GIT_PROMPT_THEME_NAME="Single_line_Solarized"
}

alias gut="git"

alias webserver="python3 -m http.server 8000 --bind 0.0.0.0"

KUBE_PS1_BINARY=oc
export PATH="$PATH:$GOPATH/bin"
#export GOROOT=$(go env GOROOT)
#export GOROOT="/home/bmillemathias/Code/go/"

function oc-kill-pods-node() {
    [[ -z "$1" ]] && return 1
    [[ $(oc get node "$1" 1>/dev/null 2>&1) -ne 0 ]] && return 1
    oca delete pods --all-namespaces -o name --grace-period=0 --force=true --field-selector spec.nodeName="$1"
}

function oc-debug-node() {
    [[ -z "$1" ]] && return 1
    oca debug --image=dockerhub.rnd.amadeus.net:5004/lbxad/data-fabric/ocp4/rhel-tools "node/${1}"
}

alias kubectl=oc
alias oca="oc --as system:admin"
alias cephtb="oc --as system:admin -n rook-ceph exec -it \$(kubectl -n rook-ceph get pod -l \"app=rook-ceph-tools\" -o jsonpath='{.items[0].metadata.name}') bash"
alias cephadmin="kubectl --as system:admin -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath=\"{['data']['password']}\" | base64 --decode && echo"
source <(oc completion bash)
